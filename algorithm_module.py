import pandas as pd
import numpy as np
import time as t
from random import uniform,randint

from sklearn.preprocessing import label_binarize
from sklearn.model_selection import RandomizedSearchCV as randomized

from sklearn.metrics import roc_curve, auc
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

from sklearn.neural_network import MLPClassifier as MLP
from sklearn.tree import DecisionTreeClassifier as DT
from sklearn.ensemble import RandomForestClassifier as RFC    
from sklearn.naive_bayes import GaussianNB as GNB
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression as LR

import warnings
warnings.filterwarnings("ignore",category=UserWarning)
warnings.filterwarnings("ignore", category=RuntimeWarning)

###################PARAMETRY KLASYFIKATORÓW - TUNING #################################################
my_cv = 2
my_n_iter = 50
tuning_params=[]
tuning_params.append(dict(hidden_layer_sizes=[(randint(20,500),),(randint(20,500),randint(20,500),),
                                            (randint(20,500),randint(20,500),randint(20,500),)],
                      activation=['identity','logistic','tanh','relu'],
                      solver=['adam','lbfgs','sgd'],
                      alpha=[1,1e-2,1e-4,1e-6,1e-8],#uniform(0.001, 0.04)
                      learning_rate=['adaptive','invscaling','constant'],
                      learning_rate_init=[0.00001,0.0001,0.001,0.005,0.01]))
tuning_params.append(dict(criterion=['gini','entropy'],
                      splitter=['best'],
                      max_depth=[None,randint(1,2000)],
                      min_samples_split=[2,3,4,5,7,10],#min_samples_split=[2,3,4,5,6,8,10,12,14,16],
                      min_samples_leaf=[1,2,3,4,5],#min_samples_leaf=[1,2,3,4,5,6,7,8,9,10],
                      max_features=['auto','sqrt',None,'log2']))
tuning_params.append(dict(n_estimators=[100,200,500,1000,2000],
                      criterion=['gini','entropy'],
                      max_features=['auto','log2'],
                      max_depth=[None,randint(1,2000)],
                      min_samples_split=[2,3,4,5,7,10],
                      min_samples_leaf=[1,2,3,4,5]))
tuning_params.append(dict(var_smoothing=[1e-11,1e-10,1e-9,1e-8,1e-7,1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,1]))
tuning_params.append(dict(n_neighbors=[3,5,7,9,11],
                      weights=['distance','uniform'],
                      algorithm=['auto','kd_tree','ball_tree','brute'],
                      leaf_size=[10,20,30,40,50],
                      p=[1,2]))
tuning_params.append(dict(C=[0.001,0.01,0.1,1,10,100,1000],
                      kernel=['linear','poly','rbf','sigmoid'],
                      degree=[1,2,3,4,5,6,7],
                      gamma=['scale','auto'],
                      coef0=[0,0.0001,0.001,0.01,0.1],
                      max_iter=[100,200,400]))
tuning_params.append(dict(penalty=[None,'l2'],
                      fit_intercept=[True,False],
                      solver=['newton-cg','lbfgs','liblinear','sag','saga'],
                      C=[0.001,0.01,0.1,1,10,100,1000]))
###################PARAMETRY KLASYFIKATORÓW - TUNING #################################################

#Tuning parametrów klasyfikatorów
def tuning_hyperparams(clf_num,X_train,X_test,y_train,y_test,num_feats):
    global tuning_params, my_cv, my_n_iter
    if clf_num == 1: 
        clf_def = MLP()
    elif clf_num == 2: 
        clf_def = DT()
    elif clf_num == 3: 
        clf_def = RFC(n_jobs=3)
    elif clf_num == 4: 
        clf_def = GNB()
    elif clf_num == 5: 
        clf_def = KNN(n_jobs=3)
    elif clf_num == 6: 
        clf_def = SVC(probability=True)
    elif clf_num == 7: 
        clf_def = LR(n_jobs=3)
                
    randoms = randomized(clf_def, tuning_params[clf_num-1],n_jobs=-1,cv=my_cv,n_iter=my_n_iter,scoring = 'accuracy')
    randoms.fit(X_train, y_train)
    print("Best parameters: ",randoms.best_params_)
    print("Accuracy: ",randoms.best_score_)


###################PARAMETRY KLASYFIKATORÓW - WŁAŚCIWE USTAWIENIE#####################################
def get_clf(clf_id,data_id):
    
##    #domyślne
##    if clf_id == 1:
##        clf = MLP()
##    if clf_id == 2:
##        clf = DT()
##    if clf_id == 3:
##        clf = RFC(n_jobs=3)
##    if clf_id == 4:
##        clf = GNB()
##    if clf_id == 5:
##        clf = KNN(n_jobs=3)
##    if clf_id == 6:
##        clf = SVC(probability=True)
##    if clf_id == 7:
##        clf = LR(n_jobs=3)

    #dobrane
    if clf_id == 1:
        if data_id == 0: #ACC+GYR
            clf = MLP(hidden_layer_sizes=(583,563,),max_iter=500,solver='adam',activation='tanh',
                      alpha=1e-6,learning_rate='invscaling',learning_rate_init=0.001)
        elif data_id == 1: #ACC
            clf = MLP(hidden_layer_sizes=(301,390,),max_iter=200,solver='adam',activation='relu',
                      alpha=1e-8,learning_rate='adaptive',learning_rate_init=0.001)
        else: #GYR
            clf = MLP(hidden_layer_sizes=(402,129,212,),max_iter=200,solver='adam',activation='relu',
                      alpha=1e-8,learning_rate='invscaling',learning_rate_init=0.001)
    if clf_id == 2:
        if data_id == 0: #ACC+GYR
            clf = DT(criterion='entropy',splitter='best',max_depth=None,min_samples_split=2,min_samples_leaf=1,max_features=None)
        elif data_id == 1: #ACC
            clf = DT(criterion='entropy',splitter='best',max_depth=100,min_samples_split=4,min_samples_leaf=1,max_features=None)
        else: #GYR
            clf = DT()
    if clf_id == 3:
        if data_id == 0: #ACC+GYR
            clf = RFC(n_jobs=3,criterion='entropy',n_estimators=1500,max_depth=100,min_samples_split=2,min_samples_leaf=1,max_features='log2')
        elif data_id == 1: #ACC
            clf = RFC(n_jobs=3,criterion='gini',n_estimators=1500,max_depth=25,min_samples_split=2,min_samples_leaf=1,max_features='auto')
        else: #GYR
            clf = RFC(n_jobs=3,criterion='gini',n_estimators=1500,max_depth=25,min_samples_split=2,min_samples_leaf=1,max_features='auto')
    if clf_id == 4:
        if data_id == 0: #ACC+GYR
            clf = GNB(var_smoothing=0.001) 
        elif data_id == 1: #ACC
            clf = GNB(var_smoothing=0.0001)
        else: #GYR
            clf = GNB(var_smoothing=0.00001)
    if clf_id == 5:
        if data_id == 0: #ACC+GYR
            clf = KNN(n_jobs=3,n_neighbors=2,weights='distance',algorithm='brute',p=1) 
        elif data_id == 1: #ACC
            clf = KNN(n_jobs=3,n_neighbors=4,weights='distance',algorithm='kd_tree',p=1,leaf_size=10)
        else: #GYR
            clf = KNN(n_jobs=3,n_neighbors=5,weights='distance',algorithm='kd_tree',p=1,leaf_size=20)
    if clf_id == 6:
        if data_id == 0: #ACC+GYR
            clf = SVC(probability=True,kernel='rbf',C=10,degree=5,gamma='scale',coef0=0,max_iter=900)
        elif data_id == 1: #ACC
            clf = SVC(probability=True,kernel='rbf',C=10,degree=7,gamma='scale',coef0=0.01,max_iter=1000)
        else: #GYR
            clf = SVC(probability=True,kernel='rbf',C=10,degree=1,gamma='scale',coef0=0.001,max_iter=1000)
    if clf_id == 7:
        if data_id == 0: #ACC+GYR
            clf = LR(n_jobs=3,penalty='l2',solver='newton-cg',fit_intercept=False,C=1000,max_iter=25)
        elif data_id == 1: #ACC
            clf = LR(n_jobs=3,penalty='l2',solver='newton-cg',fit_intercept=True,C=1000,max_iter=100)
        else: #GYR
            clf = LR(n_jobs=3,penalty='l2',solver='newton-cg',fit_intercept=True,C=1000,max_iter=50)
        
    return clf
###################PARAMETRY KLASYFIKATORÓW - WŁAŚCIWE USTAWIENIE#####################################



#Oblicza prawdopodobienstwo otrzymania etykiet
def get_labels_prob(prob_df,pred_lab):
    prob_labels = []
    for i in range(0,prob_df.shape[0]):
        prob_labels.append(prob_df[pred_lab[i]].values[i])
            
    return prob_labels

#Oblicza wskaźniki klasyfikacji
def score_labels(lab,pred_lab,pred_prob):
    
    if len(pred_lab) == 1: # 1 algorytm klasyfikacji
        pred_lab = pred_lab[0]
    else: # kilka algorytmów klasyfikacji - wyznaczenie nowych predykcji klas
        df = pd.DataFrame({'TRUE':lab,'PRED 1':pred_lab[0],'PRED 2':pred_lab[1],'PRED 3':pred_lab[2],
                           'PR 0':pred_prob[0],'PR 1':pred_prob[1],'PR 2':pred_prob[2]})
        
        new_pred_lab = []
        for i in range(0,len(pred_lab[0])):
            row = df.iloc[i,4:7].to_numpy()
            id_max = np.argmax(row)
            check = df.iloc[i,id_max+1]
            new_pred_lab.append(check)
        pred_lab = new_pred_lab
        #df['NEW PRED'] = new_pred_lab
        #print(df)
        
        #for i in range(0,len(pred_lab[0])):
            #k, v = np.unique(df.iloc[i].to_numpy(), return_counts=True)
            #pairs = dict(zip(k, v))
            #sorted_pairs = sorted(pairs, key=pairs.get, reverse=True)
            #new_pred_lab.append(sorted_pairs[0])

    #dokładność
    corrected=0
    for i in range(0,len(lab)):
        if lab[i] == pred_lab[i]:
            corrected=corrected+1

    #macierz konfuzji                    
    labelNames = np.unique(lab)
    conf = pd.DataFrame(confusion_matrix(lab, pred_lab,labelNames), index=labelNames, columns=labelNames)
    pd.set_option('display.max_rows', None,'display.max_columns',None,'display.width', None)
    conf.columns.name = 'P / T'
    for i in range(0,len(conf.columns)):
        conf.rename(columns={conf.columns[i]:str(conf.columns[i]+'_t')},inplace=True)
        conf.rename(index={conf.index[i]:str(conf.index[i]+'_p')},inplace=True)
    
    #F1
    clf_report_result={}
    clf_report = classification_report(lab, pred_lab, target_names=labelNames,digits=3,output_dict=True)
    drop_list = ['accuracy','macro avg','weighted avg']
    for key in drop_list:
        if key == 'accuracy':
            clf_report.pop(key)
        else:
            clf_report_result[key] = clf_report.pop(key)
           
    report = pd.DataFrame.from_dict(clf_report,orient='index')
    report_result = pd.DataFrame.from_dict(clf_report_result,orient='index')
    report = pd.concat([report,report_result])

    #ROC-AUC
    activities=['A','B','C','D','E','F','G','H','I','J','K','L','M','O','P','Q','R','S']
    lab =  label_binarize(lab, classes=activities)
    pred_lab =  label_binarize(pred_lab, classes=activities)
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(18):
        fpr[activities[i]], tpr[activities[i]], _ = roc_curve(lab[:, i], pred_lab[:, i])
        roc_auc[activities[i]] = auc(fpr[activities[i]], tpr[activities[i]])
    roc = pd.DataFrame.from_dict(roc_auc,orient='index')
    roc.rename(columns={0:'roc auc'},inplace=True)
    roc['samples'] = report['support'].head(len(activities))
    roc['multiply'] = roc['roc auc']*roc['samples']
           
    return 100*corrected/len(lab),conf,report,roc

       
#Przeprowadzenie pojedynczej klasyfikacji wybranym algorytmem/-ami
def classify(clf_num,X_train,X_test,y_train,y_test,num_feats,data_id):

    clf = []
    if clf_num == 8: #wybór klasyfikatora hybrydowego
        clf_num = [1,3,5]
    elif clf_num == 9:
        clf_num = [1,3,6]
    elif clf_num == 10:
        clf_num = [1,5,6]
    elif clf_num == 11:
        clf_num = [3,5,6]
    elif clf_num == 12:
        clf_num = [2,3,4]
    else:
        clf_num = [clf_num] # zwykly klasyfikator

    #uczenie
    start_clf = t.perf_counter()
    for i in range(0,len(clf_num)):
        clf_def = get_clf(clf_num[i],data_id) #wybierz klasyfikator
        clf.append(clf_def)
        clf[i].fit(X_train, y_train)
    time_clf = t.perf_counter() - start_clf
    
    #klasyfikacja dla trenowania
    pred_lab_train = []
    pred_lab_train_prob = []
    start_train = t.perf_counter()
    for i in range(0,len(clf)):
        pred_lab_train.append(clf[i].predict(X_train))
        prob_df = pd.DataFrame(clf[i].predict_proba(X_train),columns=clf[i].classes_)
        pred_lab_train_prob.append(get_labels_prob(prob_df,pred_lab_train[i]))
    score_train, conf_train,report_train, roc_train = score_labels(y_train.to_numpy(),pred_lab_train,pred_lab_train_prob)
    time_train = t.perf_counter() - start_train
    
    #klasyfikacja dla testowania
    pred_lab_test = []
    pred_lab_test_prob = []
    start_test = t.perf_counter()
    for i in range(0,len(clf)):
        pred_lab_test.append(clf[i].predict(X_test))
        prob_df = pd.DataFrame(clf[i].predict_proba(X_test),columns=clf[i].classes_)
        pred_lab_test_prob.append(get_labels_prob(prob_df,pred_lab_test[i]))
    score_test, conf_test, report_test, roc_test = score_labels(y_test.to_numpy(),pred_lab_test,pred_lab_test_prob)
    time_test = t.perf_counter() - start_test

    return [score_train,score_test],[conf_train,conf_test],[time_train,time_test,time_clf],[report_train,report_test],[roc_train,roc_test]
