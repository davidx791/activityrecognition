import glob
import numpy as np
import pandas as pd
import print_module as p
from sklearn.preprocessing import MinMaxScaler

#Pobiera listę ścieżek do plików z danymi
def get_filenames_list(sensor_type,folder,file_type):
    filenames = [[],[]]
    #*.TXT files, folder TRANSF
    for sensor in sensor_type:
        for name in glob.glob('.\\dataset\\'+folder[2]+'\\watch\\'+sensor+'\\*.'+file_type[0]):
            filenames[sensor_type.index(sensor)].append(name)
                  
    return filenames

#Pobiera listę kolumn dla danych ACC i GYR
# mode: 0 - dane z kolumnami ACTIVITY I ID
# mode: 1 - dane bez kolumn  ACTIVITY I ID
def get_column_names_list(col_files,mode):
    columns = [[],[]]
    for col_file in col_files:
        with open(col_file) as file:
            for line in file:
                line = line.replace('\n','')
                columns[col_files.index(col_file)].append(line)
    if mode : 
        for i in range (0,2):
            columns[i].pop(0)
            columns[i].pop(len(columns[i])-1)
          
    return columns

#Wczytuje dane
def read_data():
    accel_datas = []
    gyro_datas = []
    folder = ['raw','arff_files','transf_data']
    file_type = ['txt','arff']
    sensor_type = ['accel','gyro']
    col_files = ['accel_col.txt','gyro_col.txt']

    cols = get_column_names_list(col_files,0) #nazwy kolumn wszystkie
    colsData = get_column_names_list(col_files,1)#nazwy kolumn bez ACTIVITY I ID
    filenames = get_filenames_list(sensor_type,folder,file_type)#ścieżki do plików

    #odczyt plików z danymi ACC i GYR
    for file in range(len(filenames[0])): 
        accel_file = filenames[0][file]
        gyro_file = filenames[1][file]
    
        accel_data = pd.read_csv(accel_file,sep=",",header=None)
        gyro_data = pd.read_csv(gyro_file,sep=",",header=None)
        accel_data.columns = cols[0]
        gyro_data.columns = cols[1]

        accel_datas.append(accel_data)
        gyro_datas.append(gyro_data)
    #p.print_data(accel_data,gyro_data,file) #wyswietla odczytane dane

    return accel_datas,gyro_datas,cols,colsData


#Przygotowuje dataset w zależności od wyboru danych, eliminuje niepoprawne dane
# data_mode: 0 - dataset z ACC i GYR
# data_mode: 1 - dataset z ACC
# data_mode: 2 - dataset z GYR
def prepare_dataset(accel_datas,gyro_datas,broken_mode,data_mode):

    if data_mode == 0:
        #Usuwanie danych z błędnymi długościami ACC i GYR
        to_del = [] 
        for i in range(0,len(accel_datas)):
            if  len(accel_datas[i]) != len(gyro_datas[i]):
                to_del.append(i)
        for i in reversed(range(0,len(to_del))):
            accel_datas.pop(to_del[i])
            gyro_datas.pop(to_del[i])

        #Usuwanie niepoprawnych danych względem etykiet ACC i GYR
        to_del = [] 
        if broken_mode == 0:
            for i in range(0,len(accel_datas)):
                sum = 0
                for j in range(0,accel_datas[i].shape[0]):
                    if accel_datas[i].iloc[j]['A_ACTIVITY'] != gyro_datas[i].iloc[j]['G_ACTIVITY']:
                        sum += 1
                        #print(j,' ',accel_datas[i].iloc[j]['A_ACTIVITY'],' ',gyro_datas[i].iloc[j]['G_ACTIVITY'])
                if sum > 0:              
                    to_del.append(i)
            for i in reversed(range(0,len(to_del))):
                accel_datas.pop(to_del[i])
                gyro_datas.pop(to_del[i])
        elif broken_mode == 1:
            for i in range(0,len(accel_datas)):
                rows = []
                for j in range(0,accel_datas[i].shape[0]):
                    if accel_datas[i].iloc[j]['A_ACTIVITY'] != gyro_datas[i].iloc[j]['G_ACTIVITY']:
                        #print(i,' ',j,' ',accel_datas[i].iloc[j]['A_ACTIVITY'],' ',gyro_datas[i].iloc[j]['G_ACTIVITY'])
                        rows.append(j)
                if len(rows) > 0:
                    to_del.append([i,rows])
            for i in range(0,len(to_del)):
                accel_datas[to_del[i][0]].drop(to_del[i][1], inplace=True)
                gyro_datas[to_del[i][0]].drop(to_del[i][1], inplace=True)
         
    accel_dataset = pd.concat(accel_datas,ignore_index=True)
    gyro_dataset = pd.concat(gyro_datas,ignore_index=True)
    
    #Przygotowanie odpowiedniego datasetu wyjściowego
    accel_dataset.pop('A_ID')
    gyro_dataset.pop('G_ID')
    gyro_dataset.pop('G_Z9')
    if data_mode == 0:
        gyro_dataset.pop('G_ACTIVITY')
        dataset_y = accel_dataset.pop('A_ACTIVITY')
        dataset_X = pd.concat([accel_dataset,gyro_dataset], axis=1, sort=False)
    elif data_mode == 1:
        gyro_dataset.pop('G_ACTIVITY')
        dataset_y = accel_dataset.pop('A_ACTIVITY')
        dataset_X = pd.concat([accel_dataset], axis=1, sort=False)        
    elif data_mode == 2:
        accel_dataset.pop('A_ACTIVITY')
        dataset_y = gyro_dataset.pop('G_ACTIVITY')
        dataset_X = pd.concat([gyro_dataset], axis=1, sort=False)
    dataset_y.name = 'ACTIVITY'
    
    return dataset_X,dataset_y

#Układa losowo indexy datasetu
def random_index_dataset(X,y):
    X['class'] = y
    X = X.sample(frac=1).reset_index(drop=True)
    y = X['class']
    X = X.drop(['class'],axis=1)
    return X,y

#Normalizuje dane
def normalize_values(X):
    mms = MinMaxScaler()
    return pd.DataFrame(data=mms.fit_transform(X), columns=X.columns)


#usun niepoprawne wartości w zbiorze
#def reduce_missing_values(accel_datas,gyro_datas,cols,colsData):
    #for i in range(len(accel_datas)):
        #df = accel_datas[i].isnull().sum()
        #for j in range(len(df)):
            #if df[j] != 0:
                #print('File ',i,' column ',j)

    #for i in range(len(gyro_datas)):
        #df = gyro_datas[i].isnull().sum()
        #for j in range(len(df)):
            #if df[j] != 0:
                #print('File ',i,' column ',j)
    
    #return accel_datas,gyro_datas,cols,colsData
