import numpy as np
import pandas as pd
import math
import algorithm_module as a
import data_module as d
import plot_module as pl
import print_module as pr
import result_module as r
import selection_module as s
import matplotlib.pyplot as plt
from sklearn.model_selection import ShuffleSplit as ss
from sklearn.model_selection import StratifiedKFold as skf
from sklearn.model_selection import StratifiedShuffleSplit as sss

#Obliczanie wskaźników badanego algorytmu    
def get_algorithm_indicators(X,y,options,ranking,kbest_features,cv_opt):
    global c, plotNumber, alg_names, amountOfAlg, tuning, data_mode, confs_all
    
    n_activities = 18
    scores_train = []
    scores_test = []
    times_train  = []
    times_test  = []
    times_clf = []
    confs_train    = []
    confs_test    = []
    reports_train    = []
    reports_test    = []
    rocs_train   = []
    rocs_test    = []
    avg_conf_train = []
    avg_conf_test = []
        
    #uszeregowanie wg rankingu
    X = X[ranking]
    X = X.iloc[:,0:kbest_features]

    c += 1
    alg_choice = options[c]
    algorithm = alg_names[alg_choice-1]
    print('\n',algorithm,',',str(kbest_features))

    if tuning:
        print('\n**TUNING**')
        my_cv = ss(n_splits=1, test_size=0.2, random_state=0)
        for train, test in my_cv.split(X):
            X_train, X_test, y_train, y_test = X.iloc[train], X.iloc[test], y.iloc[train], y.iloc[test]
            a.tuning_hyperparams(alg_choice,X_train,X_test,y_train,y_test,kbest_features)
        return algorithm,[],[],[],[],[]
    else:
        if cv_opt == 1:
            my_cv = ss(n_splits=5, test_size=0.2, random_state=0)
            train_test = my_cv.split(X)
        if cv_opt == 2:
            X,y = d.random_index_dataset(X,y) #losowe ułożenie indexów datasetu
            my_cv = skf(n_splits=5)
            train_test = my_cv.split(X,y)
        if cv_opt == 3:
            my_cv = sss(n_splits=5, test_size=0.2, random_state=0)
            train_test = my_cv.split(X,y)
        confs_plots_names = ['Train Confusion Matrix','Test Confusion Matrix']
        for train, test in train_test:
            X_train, X_test, y_train, y_test = X.iloc[train], X.iloc[test], y.iloc[train], y.iloc[test]
            scores_i,confs_i,times_i,reports_i,rocs_i = a.classify(alg_choice,X_train,X_test,y_train,y_test,kbest_features,data_mode)

            scores_train.append(scores_i[0])
            scores_test.append(scores_i[1])
            times_train.append(times_i[0])
            times_test.append(times_i[1])
            times_clf.append(times_i[2])
            confs_train.append(confs_i[0])
            confs_test.append(confs_i[1])
            reports_train.append(reports_i[0])
            reports_test.append(reports_i[1])
            rocs_train.append(rocs_i[0])
            rocs_test.append(rocs_i[1])
            
            print("\nScore(tr,ts): {:.3f}%, {:.3f}% || Time(lrn,tr,ts,{:.0f}k): {:.2f}, {:.2f}, {:.2f}, {:.2f}"
            .format(scores_i[0],scores_i[1],math.floor(X.shape[0]/1000),times_i[2],times_i[0],times_i[1],times_i[0]+times_i[1]))

##                    ##F1 and ROC_AUC
##                    print("\nReport Train: \n",reports_i[0])
##                    print("\nReport Test: \n",reports_i[1])
##                    print("\nROC Train: \n",rocs_i[0])
##                    print("\nROC Test: \n",rocs_i[1])
##                    
##                    wykresy macierzy konfuzji
##                    print("Train Confusion Matrix: \n",confs_i[0])
##                    print("Test Confusion Matrix: \n",confs_i[1])                   
##                    confs_plots = [confs_i[0],confs_i[1]]
##                    plt.figure(plotNumber)
##                    for i in range(0,len(confs_plots)): #ploting Confusion Matrix 
##                        plt.subplot(1,2,i+1)
##                        sn.heatmap(confs_plots[i], annot=True, fmt='d')
##                        plt.title(confs_plots_names[i])
##                    plotNumber += 1

            avg_conf_train.append(confs_i[0])
            avg_conf_test.append(confs_i[1])
            
        print("***************************************************************************")
        print(algorithm+','+str(kbest_features))
        print('MEAN:\n')
        print("Score(tr,ts): {:.3f}%, {:.3f}% || Time(lrn,tr,ts,{:.0f}k): {:.3f}s, {:.3f}s, {:.3f}s, {:.3f}s"
        .format(np.mean(scores_train),np.mean(scores_test),math.floor(X.shape[0]/1000),np.mean(times_clf),np.mean(times_train),np.mean(times_test),np.mean(times_train)+np.mean(times_test)))

        #MEAN F1 AND ROC_AUC
        avg_reports_train = r.mean_dataframe(reports_train)/len(reports_train)
        avg_rocs_train = r.mean_dataframe(rocs_train)/len(rocs_train)
        avg_reports_test = r.mean_dataframe(reports_test)/len(reports_test)
        avg_rocs_test = r.mean_dataframe(rocs_test)/len(rocs_test)
##                print('\nTrain F1: \n',avg_reports_train)
##                print('\nTrain ROC: \n',avg_rocs_train)
##                print('\nTest F1: \n',avg_reports_test)
##                print('\nTest ROC: \n',avg_rocs_test)
        
        
        #obliczanie podsumowujących wykresów macierzy konfuzji
        confs_avg = [avg_conf_train,avg_conf_test]
        confs_avg_names = ['Overall Train Confusion Matrix','Overall Test Confusion Matrix']
        
        for i in range(0,len(confs_avg)): #calculating and ploting Overall Confusion Matrix
            confs_avg[i] = r.mean_dataframe(confs_avg[i])
            #print("\n",confs_avg_names[i],": \n",confs_avg[i])
                
        confs_all[kbest_features] = confs_avg
                
        return algorithm, [np.mean(scores_train),np.mean(scores_test)], [np.std(scores_train),np.std(scores_test)], [np.mean(times_train),np.mean(times_test),np.mean(times_clf)], [avg_reports_train,avg_reports_test], [avg_rocs_train,avg_rocs_test]

#Ustawienie wykonywania badań
def set_options():
    global amountOfAlg,tuningMaxIter

    # dataset wejściowy
    pr.print_dataset_mode()
    data_mode = int(input("Wybierz tryb zbioru danych:\n"))

    #ranking
    sel_opt = str(input("Przeprowadzić ranking cech? y/n \n"))
    if sel_opt == 'y':
        selectors = []
        pr.print_ranking_methods()
        ranking_opt = int(input("Wybierz sposób selekcji:\n"))
        selectors.append(ranking_opt)
        select_opt = str(input("Jeszcze jakiś? y/n \n"))
        while select_opt == 'y':
            pr.print_ranking_methods()
            ranking_opt = int(input("\nWybierz sposób selekcji:\n"))
            selectors.append(ranking_opt)
            select_opt = str(input("Jeszcze jakiś? y/n \n"))

    #tuning
    opt_tuning = str(input("Przeprowadzić tuning parametrow? y/n \n"))
    if opt_tuning=='y':
        tuning = True
    else:
        tuning = False

    #Cross validation setup
    pr.print_cv_methods()
    cv_option = int(input("Wybierz tryb walidacji krzyżowej:\n"))
    
    #cechy
    num_feats = []
    feats1 = int(input("Wybierz początkową liczbę cech:\n"))
    feats2 = int(input("Wybierz końcową liczbę cech:\n"))
    step = int(input("Wybierz krok zmiany:\n"))
    while True:
        num_feats.append(feats1)
        feats1 += step
        if feats1 > feats2:
            break
           
    num_classifiers = []
    print("Dostępne sposoby rozpoznawania:")
    pr.print_algorithm_names()
    print("0 - wszystkie")
    class_opt = int(input("Wybierz sposób rozpoznawania:\n"))
    if class_opt == 0:
        for i in range(1,amountOfAlg+1):
            num_classifiers.append(i)
        opt = 'n'
    else:
        num_classifiers.append(class_opt)
        opt = str(input("Jeszcze jakiś? y/n \n"))
        while opt == 'y':
            pr.print_algorithm_names()
            class_opt = int(input("\nWybierz sposób rozpoznawania:\n"))
            num_classifiers.append(class_opt)
            opt = str(input("Jeszcze jakiś? y/n \n"))
               
    options = [1]
    if sel_opt=='y':
        options.append(2) 
    for i in range(0,len(num_classifiers)): 
        for j in range(0,len(num_feats)): 
            options.append(3)
            options.append(num_feats[j])
            options.append(num_classifiers[i])
        if not tuning:
            options.append(4) 
    options.append(5) 
    print(options)

    return options,data_mode,sel_opt,selectors,tuning,cv_option,feats1,feats2,step


####################################MAIN####################################

dataset_X = []
dataset_y = []
ranking = []
options = []
confs_all = dict()
plotNumber = 3
style = 0
c = 0
broken_mode = 0

alg_names = ['NN','DT','RF','NB','KNN','SVM','LR','NN+RF+KNN','NN+RF+SVM','NN+KNN+SVM','RF+KNN+SVM','test']
amountOfAlg = len(alg_names)

chart_data = pl.init_chart_data()
result_dataframe_test = r.init_dataframe()
roc_dataframe_test = r.init_dataframe()
ymin,width,ymax_time = pl.plot1_2_init()

options,data_mode,sel_opt,selectors,tuning,cv_option,features1,features2,steps = set_options()

while True:
    if c < len(options):
        choice = str(options[c])
    else:
        choice = input ("\nWprowadź '9' aby zakończyć: ")
    if choice == "1":
        print("Wczytywanie danych")
        accel_datas,gyro_datas,cols,colsData = d.read_data()
        dataset_X,dataset_y = d.prepare_dataset(accel_datas,gyro_datas,broken_mode,data_mode)
        #print('X: \n',dataset_X,'\ny: \n',dataset_y,'\n')
        dataset_X = d.normalize_values(dataset_X)
        print(dataset_X)
        print('Wczytano! - ',len(dataset_y),' rekordów')
    elif choice == "2":
        print("Selekcja cech")
        features = dataset_X.shape[1]  
        ranking = s.feature_selection(dataset_X,dataset_y,features,selectors)
        print('\nFeature ranking: ',ranking)
    elif choice == "3":
        print("Rozpoznawanie aktywności")
        if len(ranking) == 0:
            ranking = dataset_X.columns
        c += 1
        kbest_features = options[c] 
        algorithm,result,result_std,result_times,results4class,result_rocs = get_algorithm_indicators(dataset_X,dataset_y,options,ranking,kbest_features,cv_option)
        if not tuning:
            chart_data = pl.add_chart_data(chart_data,algorithm,kbest_features,result,result_std,result_times)
            result_dataframe_test = r.add_f1_dataframe(result_dataframe_test,results4class[1],kbest_features,'F1 test')
            roc_dataframe_test = r.add_roc_dataframe(roc_dataframe_test,result_rocs[1],kbest_features,'ROC test')
    elif choice == "4":
        print("Rysowanie wykresu")
        ymin,ymax_time,width,feats1,style = pl.draw_chart(chart_data,style,ymin,ymax_time,width,kbest_features)
        feats2 = pr.print_dataframe_infos(result_dataframe_test,'F1')
        feats3 = pr.print_dataframe_infos(roc_dataframe_test,'ROC AUC')
        plotNumber = pl.draw_best_conf(confs_all,feats1,feats2,feats3,plotNumber,algorithm)
        chart_data = pl.init_chart_data()
        result_dataframe_test  = r.init_dataframe()
        roc_dataframe_test  = r.init_dataframe()
        confs_all = dict()
    elif choice == "5":
        if not tuning:
            plt.show()
    elif choice == "9":
        quit()
    else:
        print("Niepoprawny wybór. Wprowadź jeszcze raz.")
    c += 1
