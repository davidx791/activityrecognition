import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
import print_module as pr

colorNames = ['red','lime','blue','yellow','fuchsia','aqua','silver','darkblue','gold','springgreen']
lineStyle = ['-','--','-.',':','-','--','-.',':','-','--']
markerStyle = ['o','v','^','<','>','*','p','x','+','D']

#Inicjalizacja wykresów dokładności i czasów
def plot1_2_init():
    #Dokładności
    plt.figure(1)
    plt.subplot(1,2,1)
    label_plot(1,'Train')
    plt.subplot(1,2,2)
    label_plot(1,'Test')
    plt.subplots_adjust(left=0.07,right=0.88)
    
    #Czasy
    plt.figure(2)
    plt.subplot(1,3,1)
    label_plot(2,'learning')
    plt.subplot(1,3,2)
    label_plot(2,'training')
    plt.subplot(1,3,3)
    label_plot(2,'testing')
    plt.subplots_adjust(left=0.07,right=0.88)
    
    return 100,5,[0,0,0] #ymin,width,ymax_time

#Opisy dla wykresów
def label_plot(acc_tim,lr_tr_ts):
    plt.xlabel('Number of features')
    if acc_tim == 1:
        plt.ylabel('Accuracy [%]')
        plt.title('Quality of classification ('+str(lr_tr_ts)+')')
    if acc_tim == 2:
        plt.ylabel('Time [s]')
        plt.title('Times of '+str(lr_tr_ts))
        

#Inicjalizacja ramki danych dla wykresów
def init_chart_data():
    data = pd.DataFrame(index = ['y_train','y_test','y_std_train','y_std_test',
                                    'times_clf','times_train','times_test','times_tr+ts'])
    data.columns.name = ''
    return data

#Dodawanie nowych danych do ramki danych dla wykresów
def add_chart_data(data,alg,x,y,y_std,times):
    if data.columns.name == '':
        data.columns.name = str(alg)
    data[x] = [y[0],y[1],y_std[0],y_std[1],times[2],times[0],times[1],times[0]+times[1]]
    print('\n',data)
    return data

#Rysowanie macierzy konfuzji dla najlepszych wartości dokładności, F1 i ROC AUC
def draw_best_conf(confs_all,n1,n2,n3,num_plot,algorithm):
    titles = ['Overall Train Confusion Mat - ','Overall Test Confusion Mat - ']
    conf_dict = dict()
    n = [n1,n2,n3]
    names = ['accuracy ','F1 ','ROC AUC']
    for i in range(0,len(n)):
        conf_dict.setdefault(n[i],[]).append(names[i])
            
    for i in conf_dict.keys():
        plt.figure(num_plot)
        for j in range(2):
            plt.subplot(1,2,j+1)
            sn.heatmap(confs_all[i][j], annot=True, fmt='d',annot_kws={"fontsize":6},xticklabels=True, yticklabels=True)
            plt.title(titles[j]+algorithm+' - best: '+str(i)+' '+str(conf_dict[i]))
        num_plot += 1
    return num_plot

#Rysowanie wykresów dokładności i czasów
def draw_chart(data,style,ymin,ymax_time,width,num_feat):
    
    x = data.columns.to_numpy()
    y_train = data.loc['y_train'].to_numpy()
    y_std_train = data.loc['y_std_train'].to_numpy()
    y_test = data.loc['y_test'].to_numpy()
    y_std_test = data.loc['y_std_test'].to_numpy()
    times_train = data.loc['times_train'].to_numpy()
    times_test = data.loc['times_test'].to_numpy()
    times_clf = data.loc['times_clf'].to_numpy()

    if ymin > min(y_train):
        ymin = min(y_train)
    if ymin > min(y_test):
        ymin = min(y_test)

    if ymax_time[0] < max(times_clf):
        ymax_time[0] = max(times_clf)*1.05
    if ymax_time[1] < max(times_train):
        ymax_time[1] = max(times_train)*1.05
    if ymax_time[2] < max(times_test):
        ymax_time[2] = max(times_test)*1.05

    kbest_feats=pr.print_chart_infos(x,y_train,y_test)
    
    #Wykres - dokładności
    plt.figure(1)
    plt.subplot(1,2,1)
    plt.plot(x,y_train,linewidth = 2,markersize=width,marker=markerStyle[style],linestyle=lineStyle[style],color = colorNames[style],label=data.columns.name)
    plt.fill_between(x,[a + b for a, b in zip(y_train, y_std_train)],
                    [a - b for a, b in zip(y_train, y_std_train)],
                     alpha=0.2, color = colorNames[style])
    plt.ylim(ymin-0.5,100.5)
    plt.subplot(1,2,2)
    plt.plot(x,y_test,linewidth = 2,markersize=width,marker=markerStyle[style],linestyle=lineStyle[style],color = colorNames[style],label=data.columns.name)
    plt.fill_between(x,[a + b for a, b in zip(y_test, y_std_test)],
                    [a - b for a, b in zip(y_test, y_std_test)],
                     alpha=0.2, color = colorNames[style])
    plt.ylim(ymin-0.5,100.5)
    plt.legend(bbox_to_anchor=(1.01, 0.5), loc='center left', ncol=1)

    #Wykres - czasy
    plt.figure(2)
    plt.subplot(1,3,1)
    plt.plot(x,times_clf,linewidth = 2, markersize=width,marker=markerStyle[style],linestyle=lineStyle[style],color = colorNames[style],label=data.columns.name)
    plt.ylim(0,ymax_time[0])
    
    plt.subplot(1,3,2)
    plt.plot(x,times_train,linewidth = 2,markersize=width,marker=markerStyle[style],linestyle=lineStyle[style],color = colorNames[style],label=data.columns.name)
    plt.ylim(0,ymax_time[1])
        
    plt.subplot(1,3,3)
    plt.plot(x,times_test, linewidth = 2,markersize=width,marker=markerStyle[style],linestyle=lineStyle[style],color = colorNames[style],label=data.columns.name)
    plt.ylim(0,ymax_time[2])
    plt.legend(bbox_to_anchor=(1.01, 0.5), loc='center left', ncol=1)

    style += 1
    
    return ymin,ymax_time,width-0.25,kbest_feats,style
