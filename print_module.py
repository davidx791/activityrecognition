import pandas as pd
import numpy as np

#Wyświetlanie odpowiadających danych dla ACC i GYR
def print_data(acc,gyr):
    print('\nFILE 0-',len(acc)-1,': ')
    for i in range(len(acc)):
        print('ACC: ',i)
        print(acc[i])
        print('GYR: ',i)
        print(gyr[i])
        print('\n\n')

#Wyświetlanie podsumowujących wyników na podstawie ramki danych
def print_dataframe_infos(data,name):
    max_id = data.loc['wg avg'].astype('float64').idxmax()
    print("Info {}: max_wg_avg: {:.4f} | avg: {:.4f} | min: {}({:.4f}) | max: {}({:.4f}) |feats: {:.0f}"
        .format(name,data[max_id]['wg avg'],data[max_id]['avg'],data[max_id]['min_id'],
                data[max_id]['min'],data[max_id]['max_id'],data[max_id]['max'],max_id))
    return max_id

#Wyświetlanie podsumowujących wyników na podstawie danych na wykresie
def print_chart_infos(x,y_tr,y_ts):
    y_ts_max = max(y_ts)
    y_ts_mean = np.mean(y_ts)
    max_id = np.argmax(y_ts)
    print("Info accuracy: max_test: {:.3f} | mean_test: {:.3f} | train: {:.2f} | feats: {:.0f}"
        .format(y_ts_max,y_ts_mean,y_tr[max_id],x[max_id]))
    return x[max_id]

#Wyświetlanie nazw algorytmów    
def print_algorithm_names():
    print("1 - NeuralNetwork")
    print("2 - DecissionTree")
    print("3 - RandomForest")
    print("4 - NaiveBayes")
    print("5 - kNN")
    print("6 - SVM")
    print("7 - LR")
    print("8 - NN+RF+KNN")
    print("9 - NN+RF+SVM")
    print("10 - NN+KNN+SVM")
    print("11 - RF+KNN+SVM")

def print_ranking_methods():
    print("1 - Chi2")
    print("2 - RandomForest")
    print("3 - Pearson")
    print("4 - ANOVA")

def print_dataset_mode():
    print("0 - ACC + GYR")
    print("1 - ACC")
    print("2 - GYR")

def print_cv_methods():
    print("1 - Shuffle & Split")
    print("2 - Stratified K-Fold")
    print("3 - Stratified Shuffle & Split")

