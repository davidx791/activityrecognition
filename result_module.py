import pandas as pd
import numpy as np

#Uśrednianie wielu ramek danych
def mean_dataframe(dataframe_list):
    list_size = len(dataframe_list)
    for i in range(0,list_size-1):
        dataframe_list[i+1] = dataframe_list[i+1].add(dataframe_list[i],fill_value=0)

    return dataframe_list[list_size-1]

#Inicjalizacja wynikowej ramki danych
def init_dataframe():
    dataframe_labels = ['A','B','C','D','E','F','G','H','I','J','K','L','M','O','P','Q','R','S','avg','wg avg','min','min_id','max','max_id']
    data = pd.DataFrame(index = dataframe_labels)
    data.columns.name = ''
    return data

#Dodawanie nowej kolumny do ramki danych F1
def add_f1_dataframe(data,result,x,name):
    label_names = data.head(18).index
    data[x] = result['f1-score']
    data[x]['avg'] = result['f1-score']['macro avg']
    data[x]['wg avg'] = result['f1-score']['weighted avg']
    f1_data = result['f1-score'][label_names]
    data[x]['min'] = f1_data.min()
    data[x]['min_id'] = f1_data.idxmin()
    data[x]['max'] = f1_data.max()
    data[x]['max_id'] = f1_data.idxmax()
    pd.options.display.float_format = '{:.4f}'.format
    pd.set_option('mode.chained_assignment', None)
    print('\n',name,':\n',data)
    
    return data

#Dodawanie nowej kolumny do ramki danych ROC-AUC
def add_roc_dataframe(data,roc,x,name):
    label_names = data.index
    data[x] = roc['roc auc']
    data[x]['avg'] = sum(roc['roc auc'])/len(label_names)
    data[x]['wg avg'] = sum(roc['multiply'])/sum(roc['samples'])
    roc_auc_column = roc['roc auc'][label_names]
    data[x]['min'] = roc_auc_column.min()
    data[x]['min_id'] = roc_auc_column.idxmin()
    data[x]['max'] = roc_auc_column.max()
    data[x]['max_id'] = roc_auc_column.idxmax()
    pd.options.display.float_format = '{:.4f}'.format
    pd.set_option('mode.chained_assignment', None)
    print('\n',name,':\n',data)
    
    return data
