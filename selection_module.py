import pandas as pd
import numpy as np

from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import RandomForestClassifier as RFC
from sklearn.preprocessing import LabelEncoder as LE
from sklearn.feature_selection import f_classif
from sklearn.model_selection import train_test_split as tts

#Skalowanie danych (0-100)
def scale_values(list_x):
    x_min = min(list_x)
    x_max = max(list_x)
    return [100*(x-x_min)/(x_max-x_min) for x in list_x]

#Selekcja metodą Chi2
def select_features_chi2(data_X,data_y,num_feats):
    model = SelectKBest(score_func=chi2, k=num_feats)
    model.fit(data_X,data_y)
    return model.scores_

#Selekcja metodą Random Forest
def select_features_rfc(data_X,data_y,num_feats):
    model = RFC(max_features=num_feats, n_jobs=-1)
    model.fit(data_X,data_y)
    return model.feature_importances_

#Selekcja metodą Pearsona
def select_features_pearson(data_X,data_y,num_feats):
    Xy = data_X.copy()
    Xy['ACTIVITY'] =  LE().fit_transform(data_y)
    corr = abs(Xy.corr()['ACTIVITY'])
    corr = corr.drop(['ACTIVITY'], axis=0)
    return corr.to_numpy()

#Selekcja metodą Anova
def select_features_anova(data_X,data_y,num_feats):
    X_train, X_test, y_train, y_test = tts(data_X, data_y, test_size=0.33, random_state=1)
    model = SelectKBest(score_func=f_classif, k=num_feats)
    model.fit(X_train,y_train)
    model.transform(X_train)
    model.transform(X_test)

    return model.scores_

#Ranking cech
def get_features_ranking(cols, score, n):
    ranking = []
    pairs = zip(cols, score)
    pairs = sorted(pairs, key=lambda x: x[1])
    pairs = list(reversed(pairs))
    for pair in pairs[0:n]:
        print ('{:<25} {}'.format(pair[0], pair[1]))
        ranking.append(pair[0])
        
    return ranking

#Przeprowadzenie selekcji wybranymi metodami
def feature_selection(X,y,kbest_features,sel):
    print("Tworzenie rankingu ..")
    size_sel=len(sel)
    data = {'Cecha':X.columns}
    for i in range(0,size_sel):
        if sel[i] == 1:
            score1 = select_features_chi2(X,y,kbest_features)
            print('chi2: ',score1)
            data['Chi2']=scale_values(score1)
        if sel[i] == 2:
            score2 = select_features_rfc(X,y,kbest_features)
            print('rfc: ',score2)
            data['RFC']=scale_values(score2)
        if sel[i] == 3:
            score3 = select_features_pearson(X, y, kbest_features)
            print('pearson: ',score3)
            data['Pearson']=scale_values(score3)
        if sel[i] == 4:
            score4 = select_features_anova(X, y, kbest_features)
            print('anova: ',score4)
            data['Anova']=scale_values(score4)

    results = pd.DataFrame(data)
    results['Mean'] = results.mean(1)
    pd.set_option('display.max_rows', None,'display.max_columns',None,'display.width', None)
    
    return get_features_ranking(X.columns.to_numpy(), results['Mean'].values, kbest_features)


##from sklearn.feature_selection import mutual_info_classif
##def select_features_mutual(data_X,data_y,num_feats):
##    X_train, X_test, y_train, y_test = tts(data_X, data_y, test_size=0.33, random_state=1)
##    model = SelectKBest(score_func=mutual_info_classif, k=num_feats)
##    model.fit(X_train,y_train)
##    model.transform(X_train)
##    model.transform(X_test)
##
##    return model.scores_
##
##def select_features_roc(data_X,data_y,num_feats):
##    # import the DecisionTree Algorithm and evaluation score.
##    from sklearn.tree import DecisionTreeClassifier
##    from sklearn.metrics import roc_auc_score
##
##    # list of the resulting scores.
##    roc_values = []
##
##    X_train, X_test, y_train, y_test = tts(data_X, data_y, test_size=0.33, random_state=1)
##    # loop over all features and calculate the score.
##    for feature in X_train.columns:
##        clf = DecisionTreeClassifier()
##        clf.fit(X_train[feature].to_frame(), y_train)
##        y_scored = clf.predict_proba(X_test[feature].to_frame())
##        roc_values.append(roc_auc_score(y_test, y_scored[:, 1]))
##
##    # create a Pandas Series for visualisation.
##    roc_values = pd.Series(roc_values)
##    roc_values.index = X_train.columns
##
##    # show the results.
##    print(roc_values.sort_values(ascending=False))
##
##    return roc_values.sort_values(ascending=False)
##
##from scipy.stats import kendalltau 
##def select_features_kendall(data_X,data_y,num_feats):
##
##    kendall_values = []
##    for feature in data_X.columns:
##    # Calculating Kendall Rank correlation 
##        corr, _ = kendalltau(data_X[feature], data_y) 
##        print('Kendall Rank correlation: %.5f' % corr) 
##        kendall_values.append(corr)
##    return kendall_values
##
###Aktualizowanie ogólny wynik punktów dla cechy
##def update_feature_score(score,selected,columns):
##    for i in range (0,len(selected)):
##        for j in range (0,len(columns)):
##            if selected[i] == columns[j]:
##                score[j] = score[j] + 1
##    return score
